import { data } from '../utils'

export const chain = (n: number) => (ss: string[]) => (x: string, i: number) =>
  n === 0 ? x !== ss[i + 1] : ss[i = 1] !== chain(n - 1)(ss, x, i)

const fourteenChain = chain(14)(data)

console.log(
  data
    .map(fourteenChain)
    .indexOf(true) + 4
)
