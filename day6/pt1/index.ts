import { data, fourChain } from '../utils'

console.log(
  data
    .map(fourChain(data))
    .indexOf(true) + 4
)
