import { data, sumList } from "../utils"

const result = Math.max(...data.map(sumList))

console.log(result)