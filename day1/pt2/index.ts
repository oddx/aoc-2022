import { add, data, sortFn, sumList } from "../utils"

const result = data.map(sumList).sort(sortFn).slice(-3).reduce(add)

console.log(result)