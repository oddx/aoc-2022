import { add } from '../../utils'
import { compress, data, chunk, translate, chars } from '../utils'

const intersect = ([s1, s2, s3]: string[]): string[] => 
  chars(s2)
    .filter(Set.prototype.has, new Set(chars(s1)))
    .filter(Set.prototype.has, new Set(chars(s3)))

console.log(
  chunk(data, 3)
  .map(intersect)
  .map(compress)
  .map(translate)
  .reduce(add)
)