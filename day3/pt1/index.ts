import { add } from '../../utils'
import { bisect, chars, compress, data, translate } from '../utils'

export const intersect = ([s1, s2]: string[]): string[] => 
  chars(s2)
    .filter(Set.prototype.has, new Set(chars(s1)))

console.log(
  data
  .map(bisect)
  .map(intersect)
  .map(compress)
  .map(translate)
  .reduce(add)
)
