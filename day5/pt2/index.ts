import { moves, matrix, move } from '../utils'


console.log(
  moves
    .map(move(matrix)(false))
    .pop()
)
