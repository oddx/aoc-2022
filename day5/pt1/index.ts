import { move, moves, matrix } from '../utils'


console.log(
  moves
    .map(move(matrix)(true))
    .pop()
)
