import { add } from '../../utils'
import { data, Round, Second } from '../utils'


const rockVs = (second: Second): number => 
  second == 'Y' ? 1 + 3 :
  second == 'Z' ? 2 + 6 :
  3 + 0

const paperVs = (second: Second): number => 
  second == 'Y' ? 2 + 3 :
  second == 'Z' ? 3 + 6 :
  1 + 0

const scissorsVs = (second: Second): number => 
  second == 'Y' ? 3 + 3 :
  second == 'Z' ? 1 + 6 :
  2 + 0

const score = ([first, second]: Round): number =>
  first == 'A' ? rockVs(second) :
  first == 'B' ? paperVs(second) :
  scissorsVs(second)

console.log(data.map(score).reduce(add))
