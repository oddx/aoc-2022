import { data, identity, superset } from '../utils'


console.log(
  data
    .map   (superset)
    .filter(identity)
    .length
)
