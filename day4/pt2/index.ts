import { data, identity, intersect } from '../utils'


console.log(
  data
    .map   (intersect)
    .filter(identity)
    .length
)
